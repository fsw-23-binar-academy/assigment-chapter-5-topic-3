const express = require("express");
const app = express();
var bodyParser = require("body-parser");
const fs = require("fs");
const morgan = require("morgan");
const DATABASE = "./db/products.json";

app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());
//to set view engine is ejs
app.set("view engine", "ejs");
app.use(morgan("dev"));

// load public folder
app.use(express.static(__dirname + "/public"));

// get data atau routing ke index
app.get("/", function (req, res) {
  fs.readFile(DATABASE, "utf-8", (err, data) => {
    res.render("index", {
      form: { id: "", name: "", price: "", action: "/add" },
      products: JSON.parse(data),
      css: "css/style.css",
    });
  });
});

// ini akan di hit dari form submit add
app.post("/add", function (req, res) {
  fs.readFile(DATABASE, "utf-8", (err, data) => {
    let products = JSON.parse(data);
    const newId = products[products.length - 1].id + 1;
    const params = {
      id: newId,
      ...req.body,
    };
    products.push(params);
    fs.writeFile(DATABASE, JSON.stringify(products), (err) => {
      console.log("data products setelah di add", products);
      res.redirect("/");
    });
  });
});

app.get("/delete/:id", (req, res) => {
  const idProduct = req.params.id;
  fs.readFile(DATABASE, "utf-8", (err, data) => {
    data = JSON.parse(data);
    let newData = data.filter((product) => {
      return product.id !== Number(idProduct);
    });
    fs.writeFile(DATABASE, JSON.stringify(newData), (err) => {
      res.redirect("/");
    });
  });
});

app.get("/edit/:id", (req, res) => {
  fs.readFile(DATABASE, "utf-8", (err, data) => {
    data = JSON.parse(data);
    const product = data.find(
      (product) => product.id === Number(req.params.id)
    );
    res.render("index", {
      form: { action: `/edit/${product.id}`, ...product },
      products: data,
      css: "../css/style.css",
    });
  });
});

app.post("/edit/:id", (req, res) => {
  const id = req.params.id;
  const dataEdit = req.body;

  fs.readFile(DATABASE, "utf-8", (err, data) => {
    data = JSON.parse(data);
    data.forEach((product) => {
      if (product.id === Number(id)) {
        product.name = dataEdit.name;
        product.price = dataEdit.price;
      }
    });

    fs.writeFile(DATABASE, JSON.stringify(data), (err) => {
      res.redirect("/");
    });
  });
});

app.use((err, req, res, next) => {
  console.error("error", err);
  res.status(500).json({
    status: "fail",
    errors: err.message,
  });
});

app.use((err, req, res, next) => {
  console.error("error", err);
  res.status(404).json({
    status: "not found",
    errors: err.message,
  });
});

app.listen(3000, () => {
  console.log("listening on port...", 3000);
});
